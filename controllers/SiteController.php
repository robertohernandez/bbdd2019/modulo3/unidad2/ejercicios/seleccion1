<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\lleva;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCrud(){
        return $this->render("gestion");
    }   
     
    public function actionConsulta1(){
        //mediante DAO
        $numero = yii::$app->db
                ->createCommand('SELECT count(distinct edad) FROM ciclista')
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT distinct edad FROM ciclista',
            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Dao",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    
    public function actionConsulta1a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("edad")->distinct(),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    
      
    }
    public function actionConsulta2(){
        //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo = 'Artiach'")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.2 con Dao",
            "enunciado"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("edad")->distinct()
                 ->where("nomequipo ='Artiach'"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach(sin repetidos)",
            "sql"=>" SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    
    public function actionConsulta3(){
        //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.3 con Dao",
            "enunciado"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    
    
    public function actionConsulta3a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("edad")->distinct()
                 ->where("nomequipo ='Artiach' or nomequipo ='Amore Vita'"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach o Amore vita(sin repetidos)",
            "sql"=>" SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' or nomequipo = 'Amore Vita'",
        ]);
    
      
    }
    
    
     public function actionConsulta4(){
        //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(distinct dorsal) From ciclista WHERE edad < 25 OR edad > 30")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.4 con Dao",
            "enunciado"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
            
        ]);
    }
    public function actionConsulta4a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("dorsal")->distinct()
                 ->where("edad < 25 or edad > 30"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas del equipo cuya edad sea menor de 25 o mayor que 30(sin repetidos)",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]);
    
      
    }
        public function actionConsulta5(){
        
         //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(distinct dorsal) From ciclista WHERE edad between 28 and 32 and nomequipo ='Banesto'")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT distinct dorsal From ciclista WHERE edad between 28 and 32 and nomequipo ='Banesto'",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.5 con Dao",
            "enunciado"=>"SELECT distinct dorsal From ciclista WHERE edad between 28 and 32 and nomequipo ='Banesto'",
            "sql"=>"SELECT distinct dorsal From ciclista WHERE edad between 28 and 32 and nomequipo ='Banesto'",
            
        ]);
    
    }
    
    public function actionConsulta5a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("dorsal")->distinct()
                 ->where("edad between 28 and 32 and nomequipo ='Banesto'"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y cuyo equipo sea Banesto(sin repetidos)",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo ='Banesto'",
        ]);
    
    }
     public function actionConsulta6(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(distinct nombre) From ciclista WHERE LENGTH(nombre)>8")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT distinct nombre From ciclista WHERE LENGTH(nombre)>8",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 1.6 con Dao",
            "enunciado"=>"SELECT distinct nombre From ciclista WHERE LENGTH(nombre)>8",
            "sql"=>"SELECT distinct nombre From ciclista WHERE LENGTH(nombre)>8",
            
        ]);
    
    }
    
    public function actionConsulta6a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("nombre")->distinct()
                 ->where("LENGTH(nombre)>8"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 1.6 con Active Record",
            "enunciado"=>"nombre de los ciclistas con nombres con longitud >8(sin repetidos)",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE LENGTH(nombre)>8",
        ]);
    
    }
    public function actionConsulta7(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT UCASE(nombre) nombre, dorsal FROM ciclista",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 1.7 con Dao",
            "enunciado"=>"SELECT UCASE(nombre) nombre, dorsal FROM ciclista",
            "sql"=>"SELECT UCASE(nombre) nombre, dorsal FROM ciclista",
            
        ]);
    
    }
    
    public function actionConsulta7a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("UCASE(nombre) nombre,dorsal")->distinct(),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 1.7 con Active Record",
            "enunciado"=>"nombre en mayusculas y dorsal de todos los ciclistas",
            "sql"=>"SELECT UCASE(nombre) nombre, dorsal FROM ciclista",
        ]);
    
    }
    
    
     public function actionConsulta8(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva WHERE código = 'MGE'")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.8 con Dao",
            "enunciado"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
            
        ]);
    
    }
    
    public function actionConsulta8a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => lleva::find()
              ->select('dorsal')->distinct()
             ->where("código = 'MGE'"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.8 con Active Record",
            "enunciado"=>"Ciclistas que han llevado maillot MGE(amarillo)",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    
    }
    public function actionConsulta9(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE altura > 1500")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nompuerto FROM puerto WHERE altura > 1500",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 1.9 con Dao",
            "enunciado"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
            
        ]);
    
    }
    
    public function actionConsulta9a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('nompuerto')->distinct(),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 1.9 con Active Record",
            "enunciado"=>"nombre de los puertos con altura superior a 1500 mts",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    
    }
    
    public function actionConsulta10(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente >8 OR altura BETWEEN 1800 AND 3000")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM puerto WHERE pendiente >8 OR altura BETWEEN 1800 AND 3000",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.10 con Dao",
            "enunciado"=>"Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente >8 OR altura BETWEEN 1800 AND 3000",
            
        ]);
    
    }
    
    public function actionConsulta10a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('dorsal')->distinct()
              ->where("pendiente >8 OR altura BETWEEN 1800 AND 3000"),   
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.10 con Active Record",
            "enunciado"=>"Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente >8 OR altura BETWEEN 1800 AND 3000",
        ]);
    
    }
    
    public function actionConsulta11(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente >8 and altura BETWEEN 1800 AND 3000")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM puerto WHERE pendiente >8 and altura BETWEEN 1800 AND 3000",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.11 con Dao",
            "enunciado"=>"Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente >8 AND altura BETWEEN 1800 AND 3000",
            
        ]);
    
    }
    
    public function actionConsulta11a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('dorsal')->distinct()
              ->where("pendiente >8 AND altura BETWEEN 1800 AND 3000"),   
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.11 con Active Record",
            "enunciado"=>"Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 Y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente >8 AND altura BETWEEN 1800 AND 3000",
        ]);
    
    }
    
    
}
