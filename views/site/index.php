<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Seleccion 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de seleccion 1</h1>

        <p class="lead">Modulo 3 - unidad 2</p>
        
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.2</h3>
                        <p>Listar las edades de los ciclistas de Artiach (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
              
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore vita(sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30(sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta4'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
         
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.5</h3>
                        <p>Listar los dorsales de los ciclistas WHERE edad BETWEEN 28 AND 32 AND nomequipo ='Banesto'</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
   
            
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.6</h3>
                        <p>Listar los nombres de los ciclistas cuyo nombre tenga mas de 6 caracteres</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.7</h3>
                        <p>Listar los nombres de los ciclistas en mayusculas y su dorsal</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.8</h3>
                        <p>Listar los ciclistas que han llevado el maillot MGE(amarillo) en alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
             
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.9</h3>
                        <p>Listar los puertos cuya altura sea mayor que 1500</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.10</h3>
                        <p>Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.11</h3>
                        <p>Dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 Y cuya altura este entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta11'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
        

    </div>
</div>
